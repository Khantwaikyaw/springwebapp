package com.achromax;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by aungthumoe on 12/07/2017.
 */
@RestController
public class HelloController {
    @RequestMapping(value = "/hello")
    public String hello() {
        return "Greeting from spring boot";
    }
    
    @RequestMapping(value="/greeting")
    public String greeting(@RequestParam(name = "name", required=false) String name){
        return "hello: " + name;
    }
    
    @RequestMapping(value="/users/{id}")
    public String pathVar(@PathVariable Integer id){
            return "User Id: " + id;
        }
}
